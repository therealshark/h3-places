export default function range(to, from = 0) {
  return Array(to - from)
    .fill()
    .map((_, i) => i + from);
}
