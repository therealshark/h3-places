# H3 r/place template

## Install dependencies

`npm i`

## Start dev server

`npm run dev`

## Build for deployment

`npm run build`
